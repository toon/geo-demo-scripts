#!/bin/sh

set -eu

if [ "x$1" = "x" ]; then
	echo "Please run as $0 <domain>"
	exit 1
fi

export DOMAIN="$1"
export EXTERNAL_URL_HTTP="http://$DOMAIN"
export EXTERNAL_URL_HTTPS="https://$DOMAIN"

echo "Setting up server as a GitLab node with external URL $EXTERNAL_URL_HTTPS"

## Necessary dependencies
apt-get install -y curl openssh-server ca-certificates openssl git

## Install GitLab. From https://about.gitlab.com/installation/#debian
# Use nightlies for now!
curl https://packages.gitlab.com/install/repositories/gitlab/nightly-builds/script.deb.sh | bash
# Start with HTTP URL so we can bootstrap to HTTPS
export EXTERNAL_URL="$EXTERNAL_URL_HTTP"

# dpkg -i gitlab.deb to use a custom build
apt-get install gitlab-ee

unset EXTERNAL_URL

## Set up empty (and fake) filesystem mounts
mkdir -p /mnt/shard-1/repositories
mkdir -p /mnt/shard-2/repositories
# echo "
# TODO: custom shard config here
# " >> /etc/gitlab/gitlab.rb

## Set up database SSH key lookups
mkdir -p /opt/gitlab-shell
echo '#!/bin/bash

if [[ "$1" == "git" ]]; then
  /opt/gitlab/embedded/service/gitlab-shell/bin/authorized_keys $2
fi
' > /opt/gitlab-shell/authorized_keys
chmod a+x /opt/gitlab-shell/authorized_keys
chown root:git /opt/gitlab-shell/authorized_keys
chmod 0750 /opt/gitlab-shell/authorized_keys

echo '
AuthorizedKeysCommand /opt/gitlab-shell/authorized_keys %u %k
AuthorizedKeysCommandUser git
' >> /etc/ssh/sshd_config
service sshd reload

## Get a certificate from lets-encrypt
mkdir -p /etc/dehydrated
mkdir -p /var/lib/dehydrated/acme-challenges
echo "
BASEDIR=/var/lib/dehydrated
DOMAINS_TXT=/etc/dehydrated/domains.txt
WELLKNOWN=/var/lib/dehydrated/acme-challenges
" > /etc/dehydrated/config

curl -L https://raw.githubusercontent.com/lukas2511/dehydrated/master/dehydrated > /usr/local/bin/dehydrated
chmod a+x /usr/local/bin/dehydrated
dehydrated --register --accept-terms
echo "$DOMAIN" > /etc/dehydrated/domains.txt

echo "
nginx['custom_gitlab_server_config'] = 'location /.well-known/acme-challenge { alias /var/lib/dehydrated/acme-challenges; }'
" >> /etc/gitlab/gitlab.rb

gitlab-ctl reconfigure
gitlab-ctl restart

dehydrated -c

sed -i "s|$EXTERNAL_URL_HTTP|$EXTERNAL_URL_HTTPS|g" /etc/gitlab/gitlab.rb

echo "
nginx['redirect_http_to_https'] = true
nginx['ssl_certificate'] = '/var/lib/dehydrated/certs/$DOMAIN/fullchain.pem'
nginx['ssl_certificate_key'] = '/var/lib/dehydrated/certs/$DOMAIN/privkey.pem'
" >> /etc/gitlab/gitlab.rb

gitlab-ctl reconfigure
gitlab-ctl restart
