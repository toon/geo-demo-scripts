#!/bin/sh

set -eu

if [ "x$1" = "x" ] || [ "x$2" = "x" ]; then
	echo "Please run as $0 <primary-ip> <secondary-ip>"
	exit 1
fi

export PASSWORD="gitlab-geo-demo"
export PRIMARY_IP="$1"
export SECONDARY_IP="$2"

echo "Setting up postgresql replication on primary: $PRIMARY_IP <-> $SECONDARY_IP. Password: $PASSWORD"

sudo -u gitlab-psql /opt/gitlab/embedded/bin/psql -h /var/opt/gitlab/postgresql \
     -d template1 \
     -c "ALTER USER gitlab_replicator WITH ENCRYPTED PASSWORD '$PASSWORD'"

echo "
geo_primary_role['enable'] = true
postgresql['listen_address'] = '$PRIMARY_IP'
postgresql['trust_auth_cidr_addresses'] = ['127.0.0.1/32','$PRIMARY_IP/32']
postgresql['md5_auth_cidr_addresses'] = ['$SECONDARY_IP/32']
# New for 9.4: Set this to be the number of Geo secondary nodes you have
postgresql['max_replication_slots'] = 1
# postgresql['max_wal_senders'] = 10
# postgresql['wal_keep_segments'] = 10
" >> /etc/gitlab/gitlab.rb

